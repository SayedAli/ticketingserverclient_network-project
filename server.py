import torndb
import tornado.escape
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado.options import define, options
import datetime
import secrets
import MySQLdb
# import mysql.connector
import json
import os.path
# import torndb
# import tornado.escape
# import tornado.httpserver
# import tornado.ioloop
# import tornado.options
import os
from binascii import hexlify

# import tornado.web
# from tornado.options import define, options


define("port", default=1104, help="run on the given port", type=int)
define("mysql_host", default="127.0.0.1:3306", help="database host")
define("mysql_database", default="tickets", help="database name")
define("mysql_user", default="admin", help="database user")
define("mysql_password", default="mysql9612amir", help="database password")


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/signup", signup),
            (r"/login", login),
            (r"/sendticket", send_ticket),
            (r"/getticketcli", get_tickets_cli),
            (r"/getticketmod", get_tickets_mod),
        ]
        settings = dict()
        super(Application, self).__init__(handlers, **settings)
        self.db = torndb.Connection(
            host=options.mysql_host, database=options.mysql_database,
            user=options.mysql_user, password=options.mysql_password)


class BaseHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def check_userpass_validation(self, name, password_temp):

        if self.db.query("SELECT * FROM users WHERE username=%s and password=%s ", name, password_temp):
            return True
        else:
            return False

    def check_username(self, name):
        result = self.db.query("SELECT * FROM users WHERE username=%s", name)
        if result:
            return True
        else:
            return False

    def check_token(self, token_temp):
        if self.db.query("SELECT username FROM users WHERE token=%s", token_temp):
            return True
        else:
            return False

    @property
    def db(self):
        return self.application.db


class signup(BaseHandler):

    def post(self):
        username = self.get_argument('username')
        password = self.get_argument('password')
        firstname = self.get_argument('firstname')
        lastname = self.get_argument('lastname')

        if self.check_username(username):
            output = {'message': 'a user with this username exists', 'code': '404'}
            # json_data = json.dumps(data)
            self.write(output)
        else:
            self.db.execute("INSERT INTO tickets.users (username,password,firstname,lastname) values (%s,%s,%s,%s)",
                            username, password, firstname, lastname)

            output = {'message': 'Signed Up Successfully', 'code': '200'}
            self.write(output)

    def get(self, *args):
        # for i in range(0, 3):
        # print("%s",self.get_argument('username'))

        global username, lastname, password, firstname
        if len(args) < 2:
            output = [{'message': 'Bad Request - username or password not set'}, {'code': '400'}]
            self.write(output)
        elif len(args) >= 2 & len(args) <= 4:
            if len(args) == 2:
                username = args[0]
                password = args[1]
                print("%s , %d", username, password)
                firstname = None
                lastname = None
            elif len(args) == 3:
                username = args[0]
                password = args[1]
                firstname = args[2]
                lastname = None
            elif len(args) == 4:
                username = args[0]
                password = args[1]
                firstname = args[2]
                lastname = args[3]

            if self.check_username(username):
                data = {'message': 'a user with this username exists', 'code': '404'}
                json_data = json.dumps(data)
                self.write(json_data)
            else:
                # token = secrets.token_hex(20)
                # mydb = mysql.connector.connect(
                #     host="127.0.0.1:3306",
                #     user="admin",
                #     passwd="mysql9612amir",
                #     database="tickets"
                # )

                # mycursor = mydb.cursor()
                data = {'message': 'Signed Up Successfully', 'code': '200'}
                json_data = json.dumps(data)

                self.db.execute("INSERT INTO tickets.users (username,password,firstname,lastname) values (%s,%s,%s,%s)",
                                username, password, firstname, lastname)
                # sql = "INSERT INTO tickets.users (username,password,firstname,lastname) values (%s,%d,%s,%s)"
                # val = (username, password, firstname, lastname)
                # mycursor.execute(sql, val)
                #
                # mydb.commit()
                #
                # print(mycursor.rowcount, "record inserted.")
                # self.db.execute("INSERT INTO tickets.users ")

                self.write(json_data)

        else:

            output = [{'message': 'Bad Request - more than four fields set'}, {'code': '400'}]
            self.write(output)


class signup2(BaseHandler):
    def get(self, *args):
        user = self.db.get("SELECT * from users  where username='sayyedsajjad'")
        self.write(user)


class login(BaseHandler):
    def post(self, *args, **kwargs):
        username_temp = self.get_argument('username')
        password_temp = self.get_argument('password')

        if self.check_userpass_validation(username_temp, password_temp):
            token = secrets.token_hex(10)
            self.db.execute("UPDATE users SET token='" + token + "' where username='" + username_temp + "'")

            is_admin = self.db.get("SELECT administrator from users where username='" + username_temp + "'")
            output = {'message': 'Logged In Successfully', 'code': '200', 'token': token, 'is_admin': is_admin}
            self.write(output)
        else:
            output = {'message': 'Username Or Password Is Incorrect', 'code': '404'}
            self.write(output)


class send_ticket(BaseHandler):
    def post(self, *args, **kwargs):
        token = self.get_argument('token')
        if self.check_token(token):
            subject = self.get_argument('subject')
            body = self.get_argument('body')
            status = 0
            date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

            self.db.execute("INSERT INTO tickets.tickets (subject, body, token, status, date) "
                            "values (%s, %s, %s , %s, %s)"
                            , subject, body, token, status, date)

            id = self.db.get("SELECT LAST_INSERT_ID() from tickets.tickets")

            output = {'message': 'Ticket Sent Successfully', 'code': '200', 'id': id}
            self.write(output)
        else:
            output = {'message': 'Token Is Invalid', 'code': '404'}
            self.write(output)

class get_tickets_cli(BaseHandler):
    def post(self, *args, **kwargs):
        token = self.get_argument('token')
        if self.check_token(token):
            tickets = self.db.get("SELECT * from tickets.tickets where token='" + token + "'")
            rows_num = self.db.get("SELECT COUNT(*) from tickets.tickets")
            print ("tickets : %s \n rows_num : %s \n" , tickets , rows_num)
            # output = {'tickets': 'There Are -' + rows_num +'- Tickets', 'code':'200'}
            # for i in range(0 , rows_num - 1):
                # output += 'block '+ str(i) : {'subject': tickets}
class get_tickets_mod(BaseHandler):
    def post(self, *args, **kwargs):
        token = self.get_argument('token')
        # if self.check_token(token):

def main():
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
